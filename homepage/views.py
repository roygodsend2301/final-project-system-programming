from django.shortcuts import render,redirect

import os
# Create your views here.

def index(request):
    if request.method == "POST":
        nominal_rate = request.POST['nominal_rate']
        nominal_resolution = request.POST['nominal_resolution']


        if nominal_rate != "":
            os.system(
                f'echo "sysprog2019"| sudo -S ./control_psmouse.sh {nominal_rate} {retrieve_current_value("resolution")}' 
            )

        if nominal_resolution != "":
            os.system(
                f'echo "sysprog2019"| sudo -S ./control_psmouse.sh {retrieve_current_value("rate")} {nominal_resolution}'  
            )

        return redirect("home_page:homepage")

    context = {
        'current_rate': retrieve_current_value('rate'),
        'current_resolution': retrieve_current_value('resolution')
    }

    return render(request, 'index.html', context)

def retrieve_current_value(params):
    os.system(
            f'cat /sys/module/psmouse/parameters/{params} > result_{params}.txt' 
        )
    
    result = ""
    with open(f'result_{params}.txt', 'r') as file:
        result = file.read()
        file.close()
        
    return result
    
