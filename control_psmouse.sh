#!/bin/bash
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit 1
fi

if [ "$#" -ne 2 ]; then
    echo "Usage: ./control_psmouse.sh <value_rate> <value_resolution>"
    exit 1
fi

sudo rmmod psmouse
sudo insmod /lib/modules/$(uname -r)/kernel/drivers/input/mouse/psmouse.ko rate=$1 resolution=$2



