#!/bin/bash
### BEGIN INIT INFO
#Provides: django init
#Required-Start: $all
#Required-Stop:
#Default-Start: 2 3 4 5
#Default-Stop:
#Short-Description: initialize django for mouse changer app
### END INIT INFO

python3 /home/user/sysprog/final-project-system-programming/manage.py runserver 0.0.0.0:8000
